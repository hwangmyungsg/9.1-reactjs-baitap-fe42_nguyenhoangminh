import React, { Component } from 'react'

import data from './data.json';

export default class BaiTapList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data,
        };
    }

    renderList = () => {
        const {data} = this.state;

        return data.map((item, index) => {
            return (
                <div className="card itemCard" key={index}>
                    {/* <img src={item.hinhAnh} className="card-img-top" alt={item.tenPhim}/> */}
                    <div className='card-img-top' style={{backgroundImage: 'url('+item.hinhAnh+')',}}></div>
                    <div className="card-body">
                        <h5 class="card-title">{item.tenPhim}</h5>
                        <p className="card-text">{item.moTa}</p>
                    </div>
                </div>
            );
        });
    }

    render() {
        return (
            <div className='container my-3' id='BtRenderingList'>
                <div className="card">
                    <h5 className="card-header">
                        ReactJs / Rendering Elements / List
                        <span className='float-right'>FE42 / NguyenHoangMinh</span>
                    </h5>
                    <div className="card-body">
                        {this.renderList()}
                    </div>
                </div>
            </div>
        )
    }
}
