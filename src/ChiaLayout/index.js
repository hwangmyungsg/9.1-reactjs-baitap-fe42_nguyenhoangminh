import React, { Component } from 'react';
import Header from './header';
import Carousel from './carousel';
import ProductList from './productList';
import Promotion from './promotion';

class ChiaLayout extends Component {
    /** LAYOUT:
    Header
	Carousel
	ProductList
	ProductItem
        ---Ko làm phần Laptop
    Promotion
     */

    render() {
        return(
            <div>
                <Header/>
                <Carousel/>
                <ProductList/>
                <Promotion/>
            </div>
        );
    }
}

export default ChiaLayout;