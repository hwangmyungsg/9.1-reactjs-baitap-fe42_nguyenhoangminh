import React from 'react';
import './style.css';

// import ChiaLayout from './ChiaLayout';
import BaiTapList from './BaiTap-list'

function App() {
  return (
    <div>
      {/* <ChiaLayout/> */}
      <BaiTapList/>
    </div>
  );
}

export default App;
